'use strict';

exports.up = knex =>
  knex.schema.createTable('Suppliers', table => {
    table.uuid('id').primary();
    table.string('supplierName').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table
      .timestamp('deletedAt')
      .nullable()
      .defaultTo(null);
  });

exports.down = knex => knex.schema.dropTable('Suppliers');
