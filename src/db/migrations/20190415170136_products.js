'use strict';

exports.up = knex =>
  knex.schema.createTable('Products', table => {
    table.uuid('id').primary();
    table.string('productName').notNullable();
    table.string('upc').notNullable();
    table
      .integer('quantityOnHand')
      .notNullable()
      .defaultTo(0);
    table.string('storageLocation').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table
      .timestamp('deletedAt')
      .nullable()
      .defaultTo(null);
  });

exports.down = knex => knex.schema.dropTable('Products');
