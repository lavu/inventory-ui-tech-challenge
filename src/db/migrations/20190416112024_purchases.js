'use strict';

exports.up = knex =>
  knex.schema.createTable('Purchases', table => {
    table.uuid('id').primary();
    table.uuid('supplierId').unsigned();
    table.foreign('supplierId').references('Suppliers.id');
    table.uuid('productId').unsigned();
    table.foreign('productId').references('Products.id');
    table.integer('quantityReceived').notNullable();
    table.date('purchaseDate').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table
      .timestamp('deletedAt')
      .nullable()
      .defaultTo(null);
  });

exports.down = knex => knex.schema.dropTable('Purchases');
