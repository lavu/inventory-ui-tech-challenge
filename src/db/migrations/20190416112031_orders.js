'use strict';

exports.up = knex =>
  knex.schema.createTable('Orders', table => {
    table.uuid('id').primary();
    table.uuid('productId').unsigned();
    table.foreign('productId').references('Products.id');
    table.string('name').notNullable();
    table.integer('quantityShipped').notNullable();
    table.date('orderDate').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table
      .timestamp('deletedAt')
      .nullable()
      .defaultTo(null);
  });

exports.down = knex => knex.schema.dropTable('Orders');
