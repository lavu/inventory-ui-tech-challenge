'use strict';

const faker = require('faker');

exports.seed = knex =>
  knex('Suppliers')
    .del()
    .then(() => knex('Suppliers').insert(data));

const data = [
  {
    id: faker.random.uuid(),
    supplierName: `Valley Brook Farm`,
  },
  {
    id: faker.random.uuid(),
    supplierName: `Boar's Head Provision Co., Inc.`,
  },
  {
    id: faker.random.uuid(),
    supplierName: `Nestle USA Inc.`,
  },
  {
    id: faker.random.uuid(),
    supplierName: `Andrew Robbins Holdings LLC`,
  },
  {
    id: faker.random.uuid(),
    supplierName: `International Commissary Corporation`,
  },
];
