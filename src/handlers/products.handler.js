'use strict';

/**
 * Get All Products Handler
 * @param {Object} request
 * @param {Function} h
 */
function getAllProductsHandler(request, h) {
  const { products } = request.pre;
  return h.response({ products });
}

/**
 * Get Product Handler
 * @param {Object} request
 * @param {Function} h
 * @returns
 */
function getProductById(request, h) {
  const { product } = request.pre;
  return h.response({ product });
}

module.exports = {
  getAllProductsHandler,
  getProductById,
};
