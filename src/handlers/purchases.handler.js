'use strict';

/**
 * Purchase new Inventory
 * @param {Object} request
 * @param {Function} h
 */
function purchase(request, h) {
  return h.response();
}

module.exports = {
  purchase,
};
