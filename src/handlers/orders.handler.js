'use strict';

/**
 * Create an Order
 * @param {Object} request
 * @param {Function} h
 */
function createOrder(request, h) {
  return h.response();
}

/**
 * Find last Order
 * @param {Object} request
 * @param {Function} h
 */
function findLastOrder(request, h) {
  const { order } = request.pre;
  return h.response(order);
}

module.exports = {
  createOrder,
  findLastOrder,
};
