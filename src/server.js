'use strict';

require('dotenv').config();

const Hapi = require('hapi');
const Knex = require('knex');

const server = Hapi.server({
  port: process.env.NODE_PORT,
  host: process.env.NODE_HOST,
  routes: {
    cors: {
      origin: ['*'],
      additionalHeaders: ['cache-control', 'x-requested-with '],
    },
    validate: {
      failAction: (request, h, err) => {
        return err;
      },
    },
  },
});

const db = Knex({
  client: 'pg',
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
});

// ========= ROUTES ============= //
const healthCheckRoute = require('./routes/health-check.route.js');
const productsRoute = require('./routes/products.route.js');
const ordersRoute = require('./routes/orders.route.js');
const purchasesRoute = require('./routes/purchases.route.js');
server.route([
  healthCheckRoute,
  ...productsRoute,
  ...ordersRoute,
  ...purchasesRoute,
]);

exports.init = async () => {
  server.db = db;
  await server.initialize();
  return server;
};

exports.start = async () => {
  server.db = db;
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
  return server;
};

// unhandledRejection event handler
process.on('unhandledRejection', err => {
  console.error(err);
  process.exit(1);
});
