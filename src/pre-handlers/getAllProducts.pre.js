'use strict';

/**
 * Get All Products Pre
 * @param {Object} request
 * @returns {Array} products
 */
async function getAllProduct(request) {
  const { db } = request.server;

  try {
    // Find all products
    return await db
      .select('id', 'productName', 'upc', 'quantityOnHand')
      .from('Products');
  } catch (error) {
    throw error;
  }
}

module.exports = getAllProduct;
