'use strict';

const Boom = require('boom');

/**
 * Find the last time the product was ordered
 * @param {Object} request
 */
async function findLastOrder(request) {
  const { db } = request.server;
  const { productId } = request.params;

  try {
    // Inserting Order record
    const res = await db
      .select('productId', 'name', 'quantityShipped', 'orderDate')
      .max('createdAt')
      .from('Orders')
      .where({
        productId,
      })
      .groupBy('productId', 'name', 'quantityShipped', 'orderDate');

    return {
      productId: res[0].productId,
      name: res[0].name,
      quantityShipped: res[0].quantityShipped,
      orderDate: res[0].orderDate,
    };
  } catch (error) {
    throw Boom.badRequest(error);
  }
}

module.exports = findLastOrder;
