'use strict';

const uuid = require('uuid/v4');
const Boom = require('boom');

/**
 * Create an Order for a specific Product
 * @param {Object} request
 */
async function createOrder(request) {
  const { db } = request.server;
  const product = request.pre.product[0];
  const { name, quantity, orderDate } = request.payload;

  // If the quantity in our inventory is lower that the
  // requested quantity we return an Error
  if (product.quantityOnHand < quantity) {
    throw Boom.badRequest('Not enough inventory');
  }

  try {
    // Inserting Order record
    await db
      .insert({
        id: uuid(),
        productId: product.id,
        quantityShipped: quantity,
        name,
        orderDate,
      })
      .into('Orders');

    // Updating available inventory
    await db('Products')
      .update({
        quantityOnHand: product.quantityOnHand - quantity,
      })
      .where('id', '=', product.id);

    return true;
  } catch (error) {
    throw error;
  }
}

module.exports = createOrder;
