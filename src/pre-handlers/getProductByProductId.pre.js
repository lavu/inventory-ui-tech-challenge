'use strict';

const Boom = require('boom');

/**
 * Get Product by Product Id Pre
 * @param {Object} request
 * @returns {Array} product
 */
async function getProductByProductId(request) {
  const { db } = request.server;
  const { productId } = request.params;

  try {
    // Find Product using productId
    const res = await db
      .select('id', 'productName', 'upc', 'quantityOnHand')
      .from('Products')
      .where({
        id: productId,
      });

    // Check if product was found return Error if not
    if (!Array.isArray(res) || !res.length) {
      throw Boom.notFound('Product not Found');
    }

    return res;
  } catch (error) {
    throw error;
  }
}

module.exports = getProductByProductId;
