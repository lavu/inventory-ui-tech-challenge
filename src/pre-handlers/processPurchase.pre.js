'use strict';

const uuid = require('uuid/v4');
const Boom = require('boom');

/**
 * Process Purchase request for a specifict Product
 * @param {Object} request
 */
async function processPurchase(request) {
  const { db } = request.server;
  const product = request.pre.product[0];
  const { supplierId, quantityReceived, purchaseDate } = request.payload;

  try {
    // Inserting Order record
    await db
      .insert({
        id: uuid(),
        supplierId,
        productId: product.id,
        quantityReceived,
        purchaseDate,
      })
      .into('Purchases');

    // Updating available inventory
    await db('Products')
      .update({
        quantityOnHand: product.quantityOnHand + quantityReceived,
      })
      .where('id', '=', product.id);

    return true;
  } catch (error) {
    // Return error if Supplier not found
    if (error.constraint === 'purchases_supplierid_foreign')
      throw Boom.notFound('Supplier not found');

    throw error;
  }
}

module.exports = processPurchase;
