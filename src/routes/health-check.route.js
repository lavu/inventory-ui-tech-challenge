'use strict';

const healthCheckRoute = {
  method: 'GET',
  path: '/health-check',
  options: {
    handler: (request, h) => h.response(),
    description: 'Health Check',
  },
};

module.exports = healthCheckRoute;
