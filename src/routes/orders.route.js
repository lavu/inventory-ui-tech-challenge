'use strict';

const Joi = require('joi');
const getProductByProductId = require('../pre-handlers/getProductByProductId.pre.js');
const createOrder = require('../pre-handlers/createOrder.pre.js');
const findLastOrder = require('../pre-handlers/findLastOrder.pre.js');
const handler = require('../handlers/orders.handler.js');

const ordersRoute = [
  {
    method: 'POST',
    path: '/inventory/orders/{productId}',
    options: {
      pre: [
        { method: getProductByProductId, assign: 'product' },
        { method: createOrder, assign: 'order' },
      ],
      handler: handler.createOrder,
      description: 'Create an Order for selected Product',
      validate: {
        params: Joi.object().keys({
          productId: Joi.string().guid({
            version: 'uuidv4',
          }),
        }),
        payload: Joi.object().keys({
          name: Joi.string().required(),
          quantity: Joi.number().required(),
          orderDate: Joi.date().default(new Date().toLocaleString()),
        }),
      },
    },
  },
  {
    method: 'GET',
    path: '/inventory/orders/{productId}',
    options: {
      pre: [
        { method: getProductByProductId, assign: 'product' },
        { method: findLastOrder, assign: 'order' },
      ],
      handler: handler.findLastOrder,
      description: 'Find last time the product was ordered',
      validate: {
        params: Joi.object().keys({
          productId: Joi.string().guid({
            version: 'uuidv4',
          }),
        }),
      },
    },
  },
];

module.exports = ordersRoute;
