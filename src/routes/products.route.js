'use strict';

const Joi = require('joi');
const getAllProducts = require('../pre-handlers/getAllProducts.pre.js');
const getProductByProductId = require('../pre-handlers/getProductByProductId.pre.js');
const handler = require('../handlers/products.handler.js');

const productsRoute = [
  {
    method: 'GET',
    path: '/inventory/products',
    options: {
      pre: [{ method: getAllProducts, assign: 'products' }],
      handler: handler.getAllProductsHandler,
      description: 'Get list of all Products',
    },
  },
  {
    method: 'GET',
    path: '/inventory/products/{productId}',
    options: {
      pre: [{ method: getProductByProductId, assign: 'product' }],
      handler: handler.getProductById,
      description: 'Get list of all Products',
      validate: {
        params: Joi.object().keys({
          productId: Joi.string().guid({
            version: 'uuidv4',
          }),
        }),
      },
    },
  },
];

module.exports = productsRoute;
