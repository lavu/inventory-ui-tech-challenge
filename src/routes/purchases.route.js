'use strict';

const Joi = require('joi');
const getProductByProductId = require('../pre-handlers/getProductByProductId.pre.js');
const processPurchase = require('../pre-handlers/processPurchase.pre.js');
const handler = require('../handlers/purchases.handler.js');

const ordersRoute = [
  {
    method: 'POST',
    path: '/inventory/purchases/{productId}',
    options: {
      pre: [
        { method: getProductByProductId, assign: 'product' },
        { method: processPurchase, assign: 'purchase' },
      ],
      handler: handler.purchase,
      description: 'Purchase inventory for selected Product',
      validate: {
        params: Joi.object().keys({
          productId: Joi.string().guid({
            version: 'uuidv4',
          }),
        }),
        payload: Joi.object().keys({
          supplierId: Joi.string().required(),
          quantityReceived: Joi.number().required(),
          purchaseDate: Joi.date().default(new Date().toLocaleString()),
        }),
      },
    },
  },
];

module.exports = ordersRoute;
