'use strict';

const Lab = require('lab');
const { expect } = require('code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../src/server.js');

describe('GET /inventory/products', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with 200', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/inventory/products',
    });
    expect(res.statusCode).to.equal(200);
    expect(res.result).to.be.an.object();
    expect(res.result.products).to.be.an.array();
  });
});

describe('GET /inventory/products/{productId}', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with 200', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/inventory/products/0cb08519-e59f-444a-8e6e-32cef95dff88',
    });
    expect(res.statusCode).to.equal(200);
    expect(res.result).to.be.an.object();
    expect(res.result.product).to.be.an.array();
  });

  it('responds with 404 when product not found', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/inventory/products/0cb08519-e59f-444a-8e6e-32cef95dff89',
    });
    expect(res.statusCode).to.equal(404);
    expect(res.result).to.be.an.object();
    expect(res.result.message).to.equal('Product not Found');
  });

  it('responds with 400 when wrong uuid', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/inventory/products/0cb08519-e59f-444a-8e6e-32cef95dff892',
    });
    expect(res.statusCode).to.equal(400);
  });
});
