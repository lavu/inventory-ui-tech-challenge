'use strict';

const Lab = require('lab');
const { expect } = require('code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../src/server.js');

describe('GET /inventory/purchases/{productId}', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with 200', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/purchases/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        supplierId: 'a24b7c1e-633e-40ba-a8c2-ce82804f2601',
        quantityReceived: 100,
      },
    });
    expect(res.statusCode).to.equal(200);
  });

  it('responds with 404 if supplier not found', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/purchases/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        supplierId: 'a24b7c1e-633e-40ba-a8c2-ce82804f2602',
        quantityReceived: 100,
      },
    });
    expect(res.statusCode).to.equal(404);
    expect(res.result.message).to.equal('Supplier not found');
  });

  it('responds with 404 if wrong payload', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/purchases/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        supplierId: 'a24b7c1e-633e-40ba-a8c2-ce82804f2602',
        quantityReceived: 100,
      },
    });
    expect(res.statusCode).to.equal(404);
  });
});
