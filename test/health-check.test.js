'use strict';

const Lab = require('lab');
const { expect } = require('code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../src/server.js');

describe('GET /health-check', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with 200', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/health-check',
    });
    expect(res.statusCode).to.equal(200);
  });
});
