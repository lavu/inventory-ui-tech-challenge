'use strict';

const Lab = require('lab');
const { expect } = require('code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../src/server.js');

describe('GET /inventory/orders/{productId}', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with 200', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/orders/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        name: 'Company A',
        quantity: 1,
      },
    });
    expect(res.statusCode).to.equal(200);
  });

  it('find last order and respond 200', async () => {
    const productId = '0cb08519-e59f-444a-8e6e-32cef95dff88';
    const res = await server.inject({
      method: 'get',
      url: `/inventory/orders/${productId}`,
    });
    expect(res.statusCode).to.equal(200);
    expect(res.result.productId).to.equal(productId);
    expect(res.result.name).to.be.a.string();
    expect(res.result.quantityShipped).to.be.a.number();
    expect(res.result.orderDate).to.be.a.date();
  });

  it('responds with 400 if not enough inventory', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/orders/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        name: 'Company A',
        quantity: 99999999999,
      },
    });
    expect(res.statusCode).to.equal(400);
    expect(res.result.message).to.equal('Not enough inventory');
  });

  it('responds with 400 if not enough inventory', async () => {
    const res = await server.inject({
      method: 'post',
      url: '/inventory/orders/0cb08519-e59f-444a-8e6e-32cef95dff88',
      payload: {
        name: 'Company A',
      },
    });
    expect(res.statusCode).to.equal(400);
  });
});
